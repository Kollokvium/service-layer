//
//  AppDelegate.swift
//  Service Layer
//
//  Created by Anton Klymenko on 1/13/19.
//  Copyright © 2019 Anton Klymenko. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        return true
    }
}
