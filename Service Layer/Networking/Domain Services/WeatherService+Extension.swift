//
//  WeatherService.swift
//  WhishList
//
//  Created by Anton Klymenko on 1/13/19.
//  Copyright © 2019 Anton Klymenko. All rights reserved.
//

import Foundation

public enum WeatherCharacteristics {
    case temperature
}

protocol WeatherFetcherProtocol {
    func fetch(response: @escaping (WeatherModel?) -> Void)
}

struct WeatherFetcher: WeatherFetcherProtocol {
    let networking: Networking
    
    init(networking: Networking) {
        self.networking = networking
    }
    
    func fetch(response: @escaping (WeatherModel?) -> Void) {
        networking.request(from: WeatherCharacteristics.temperature) { data, error in
            if let error = error {
                print("Error received requesting Weather price: \(error.localizedDescription)")
                response(nil)
            }
            
            let decoded = self.decodeJSON(type: WeatherModel.self, from: data)
            if let decoded = decoded {
                print("Weather returned: \(decoded.weather)")
            }
            response(decoded)
        }
    }
    
    private func decodeJSON<T: Decodable>(type: T.Type, from: Data?) -> T? {
        let decoder = JSONDecoder()
        guard let data = from,
            let response = try? decoder.decode(type.self, from: data) else { return nil }
        
        return response
    }
}

extension WeatherCharacteristics: Endpoint {
    var path: String {
        switch self {
        case .temperature: return "http://api.openweathermap.org/data/2.5/weather?q=LasVegas&appid=a2e284ed02784218c7f3367c4e8ec167"
        }
    }
}
