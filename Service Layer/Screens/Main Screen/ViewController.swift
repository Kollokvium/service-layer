//
//  ViewController.swift
//  Service Layer
//
//  Created by Anton Klymenko on 1/13/19.
//  Copyright © 2019 Anton Klymenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var additionalLabel: UILabel!
    @IBOutlet weak var additionalLAbel2: UILabel!
    @IBOutlet weak var additionalLabel3: UILabel!
    
    private lazy var weatherData = WeatherFetcher(networking: HTTPSNetworking())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getWeatherData()
    }
    
    func getWeatherData() {
        self.weatherData.fetch { [weak self] (modelData) in
            DispatchQueue.main.async {
                self?.temperatureLabel.text = "Temperature " + String(describing: modelData?.main.temp ?? 0)
                self?.additionalLabel.text =  "Pressure " + String(describing: modelData?.main.pressure ?? 0)
                self?.additionalLAbel2.text = "Humidity " + String(describing: modelData?.main.humidity ?? 0)
                self?.additionalLabel3.text = "Clouds " + String(describing: modelData?.clouds.all ?? 0)
            }
        }
    }
}
